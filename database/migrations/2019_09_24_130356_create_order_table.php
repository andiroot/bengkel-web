<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('user_id')->unsigned()->index();
            $table->enum('status',['created','processing',
                        'confirmed','shipped','delivered','finished','canceled']);
            $table->timestamp('canceled_at')->nullable();
            $table->bigInteger('canceled_by')->unsigned()->index()->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('delivered')->nullable();
            $table->text('description')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();//alamat penerima
            $table->integer('ongkir')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('canceled_by')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
