<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('payment_method',['cash','online']);
            $table->enum('status',['paid','waiting','processing','expired']);
            $table->bigInteger('order_id')->unsigned()->index();
            $table->foreign('order_id')->references('id')->on('order');
            $table->integer('total')->default(0)->unsigned();
            $table->integer('total_paid')->default(0)->unsigned()->nullable();
            $table->integer('exchange')->default(0)->unsigned();
            $table->integer('discount')->default(0)->unsigned();
            $table->string('bukti_pembayaran')->nullable();
            $table->date('due_time');
            $table->timestamp('paid_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
