<?php

namespace App\Http\ViewComposers;

use Auth;
use DB;
use Illuminate\View\View;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\User;
use App\Models\Barang;
class MasterComposer
{
    
    public function compose(View $view)
    {
        //$user=DB::table('profile')->join('personal.profile','users.id','=','profile.account_id')
        //        ->where('profile.account_id', Auth::id())->first();
        
        $cartItem=Barang::
        where('cart.status','added')
                    ->where('users.id',1)                   
                ->join('cart','cart.barang_id','barang.id')
                ->join('users','cart.user_id','users.id')
                ->select('barang.nama_barang','barang.harga',
                    'barang.berat','barang.gambar','cart.jumlah','users.id as user_id'
                    ,DB::raw('barang.harga * cart.jumlah as total'),'barang.description')
                ->limit(5)
                ->get();

                 
                $items=Barang::
        where('cart.status','added')
                    ->where('users.id',1)                   
                ->join('cart','cart.barang_id','barang.id')
                ->join('users','cart.user_id','users.id')
                ->select('barang.nama_barang','barang.harga',
                    'barang.berat','barang.gambar','cart.jumlah','users.id as user_id'
                    ,DB::raw('barang.harga * cart.jumlah as total'),'barang.description')
                
                ->get();
                $sum=0;

                 foreach($items as $item){
                     $sum+=$item->total;
                 }
        
                 
                $totalBerat=[];
                $i=0;
                foreach($items as $item){
                    
                       $totalBerat[$i]=$item->berat*$item->jumlah;
                   $i++;
                }
                $berat=array_sum($totalBerat);
                
        $view->with('items',$items)
                ->with('countCart',count($items))
                ->with('sum',$sum)
                ->with('berat',$berat)
                ->with('cartItem',$cartItem);
        
    }
}