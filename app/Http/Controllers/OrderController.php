<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Barang;
use DB;
use Auth;
use Carbon\Carbon;
use Alert;
use App\Models\Cart;
use DataTables;
class OrderController extends Controller
{
    //
    public function create(Request $request)
    {
        //dd($request->all());
        

        $items=Barang::
        where('cart.status','added')
                    ->where('users.id',Auth::id())                   
                ->join('cart','cart.barang_id','barang.id')
                ->join('users','cart.user_id','users.id')
                ->select('barang.id as barangID','barang.nama_barang','barang.harga',
                    'barang.berat','barang.gambar','cart.jumlah','users.id as user_id'
                    ,DB::raw('barang.harga * cart.jumlah as total'),'barang.description')
                
                ->get();
              
        $tr=DB::transaction(function ($req) use ($request,$items){
            
            $order=Order::insert([
                'user_id'=>Auth::id(),
                'status'=>'created',
                'address'=>$request->address,
                'ongkir'=>$request->choose,
                'created_at'=>Carbon::now(),
                'phone'=>$request->phone
            ]);
            $orderId = DB::getPdo()->lastInsertId();
            
            $sum=0;
    
                     foreach($items as $item){
                         $sum+=$item->total;
                     }
            $payment=Payment::insert([
                'payment_method'=>'online',
                'status'=>'waiting',
                'order_id'=>$orderId,
                'total'=>$sum+$request->choose,//$sum+ongkir
                'due_time'=>Carbon::now()->addDays(1),
                
                'created_at'=>Carbon::now()
            ]);

            foreach($items as $item){
                Cart::where('barang_id',$item->barangID)
                    ->update(['status'=>'ordered','order_id'=>$orderId]);
            }
            
        });
        
                Alert::success('Order Berhasil Dibuat','Berhasil');
                return redirect('/browse');
    }

    public function cancel()
    {
        //ini nanti foreach trus di Barang::increment('col',$amount) 
    }

    public function myOrder()
    {
        $data=Order::join('payment','payment.order_id','order.id')
            ->select('order.user_id','order.id as order_id','order.status as order_status','order.canceled_at',
                    'order.delivered','order.created_at','payment.payment_method',
                    'payment.status as payment_status','payment.total',
                    'payment.bukti_pembayaran','payment.due_time','payment.paid_at')
            ->where('order.user_id',Auth::id())
            ->paginate(10);
            
        return view('pesanan.index')->with('data',$data);
    }

    public function invoice($id)
    {
        
        $order=Order::join('payment','payment.order_id','order.id')
                    ->join('users','users.id','order.user_id')
                ->select('order.user_id','order.id as order_id','order.status as order_status',
                        'order.canceled_at','order.created_at','payment.payment_method',
                        'payment.status as payment_status','payment.total',
                        'payment.bukti_pembayaran','payment.due_time','payment.paid_at'
                        ,'order.phone','order.address','users.email','users.name','order.ongkir')
                ->where('order.id',$id)->first();

        $items=Barang::
        where('cart.order_id',$id)
                                      
                ->join('cart','cart.barang_id','barang.id')
                ->join('users','cart.user_id','users.id')
                ->select('barang.nama_barang','barang.harga',
                    'barang.berat','barang.gambar','cart.jumlah','users.id as user_id'
                    ,DB::raw('barang.harga * cart.jumlah as total'),'barang.description',
                    'barang.id')
                ->get();

                $sum=0;

                foreach($items as $item){
                    $sum+=$item->total;
                }

        return view('pesanan.invoice')
            ->with('items',$items)
            ->with('order',$order)
            ->with('sum',$sum);
    }

    public function paid(Request $request)
    {
        
        if($request->hasFile('bukti')){
            $file=$request->file('bukti');
            $uploaded=Carbon::now();
            $extension=$file->getClientOriginalExtension();
            $filename=sha1($uploaded).'.'.$extension;
            
            $file->move('storage/app/public/',$filename);
            
            $data=[
                
                'status'=>'processing',
                'bukti_pembayaran'=>$filename,
                'paid_at'=>Carbon::now()
            ];
            Alert::success('Upload Bukti Pembayaran Berhasil','Berhasil');
            $payment=Payment::where('order_id',$request->order)->update($data);
            
        }
        else{
            dd('lol');
        }
        
        return redirect()->back();
    }
    public function getOrder()
    {
        $order=Order::join('payment','payment.order_id','order.id')
                    ->join('users','users.id','order.user_id')
                ->select('order.user_id','order.id as order_id','order.status as order_status',
                        'order.canceled_at','order.created_at','payment.payment_method',
                        'payment.status as payment_status','payment.total',
                        'payment.bukti_pembayaran','payment.due_time','payment.paid_at'
                        ,'order.phone','order.address','users.email','users.name','order.ongkir'
                        ,'order.delivered')
                        ->get();
        return DataTables::of($order)
        ->editColumn('order_id',function($order){
            return '<a href="'.route('order.invoice',$order->order_id).'">'.$order->order_id.'</a>';
        })
        ->rawColumns(['order_id'])
        ->make(true);
    }

    public function get()
    {
        return view('pesanan.data');
    }

    

    public function getOrderDetailCount($id)
    {

    }

    public function getOrderDetail($id)
    {

    }

    
}
