<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use Carbon\Carbon;
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Barang::paginate(10);
        //dd($data);
        return view('barang.index')->with('data',$data);
        //return view('barang.index')->with('data',$data);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        if($request->hasFile('gambar_barang')){
            $file=$request->file('gambar_barang');
            $uploaded=Carbon::now();
            $extension=$file->getClientOriginalExtension();
            $filename=sha1($uploaded).'.'.$extension;
            
            $file->move('storage/app/public/',$filename);
            
            $data=[
                'kode_barang'=>$request->kd_barang,
                'nama_barang'=>$request->nama_barang,
                'stock'=>$request->stock_barang,
                'harga'=>$request->harga_barang,
                'gambar'=>$filename,
                'berat'=>$request->berat,
                'description'=>$request->description,
                'created_at'=>Carbon::now()
            ];
            Barang::insert($data);
            return redirect()->back();
        }
        else{
            dd($request->hasFile('gambar_barang'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the data for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data=Barang::find($id);
        return view('barang.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       dd($request->all());
        $data=[];
        if($request->hasFile('gambar_barang')){
            $file=$request->file('gambar_barang');
            $uploaded=Carbon::now();
            $extension=$file->getClientOriginalExtension();
            $filename=sha1($uploaded).'.'.$extension;
            
            $file->move('storage/app/public/',$filename);
            
            $data=[
                
                'nama_barang'=>$request->nama_barang,
                'stock'=>$request->stock_barang,
                'harga'=>$request->harga_barang,
                'gambar'=>$filename,
                'description'=>$request->description,
                'berat'=>$request->berat,
                'updated_at'=>Carbon::now()
            ];
        }
        else{
            $data=[
                'nama_barang'=>$request->nama_barang,
                'stock'=>$request->stock_barang,
                'harga'=>$request->harga_barang,
                'description'=>$request->description,
                'updated_at'=>Carbon::now()
            ];
        }
            Barang::where('id',$id)->update($data);
            return redirect('/barang');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Barang::where('id',$id)->delete();
        return redirect('/barang');
    }

    

    public function browse()
    {
        $data=Barang::paginate(10);
       
        return view('barang.browse')->with('data',$data);
    }

    public function select($id)
    {
        $data=Barang::find($id);
        return view('barang.select')->with('data',$data);
    }
}
