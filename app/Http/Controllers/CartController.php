<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\Models\Cart;
use RajaOngkir;
use App\Models\Barang;
use DB;
use Auth;
class CartController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        return view('barang.cart');
    }

    public function add(Request $request)
    {
        
        
        $data=[
            'barang_id'=>$request->barang,
            'user_id'=>Auth::id(),
            'status'=>'added',
            'jumlah'=>$request->jumlah
        ];

        Cart::insert($data);
        Barang::where('id',$request->barang)->decrement('stock',$request->jumlah);
        Alert::success('Barang berhasil ditambahkan ke Keranjang','Berhasil');
        return redirect()->back();
    }

    public function checkout(Request $request)
    {
      
        $cartItem=Barang::
        where('cart.status','added')
                    ->where('users.id',Auth::id())                   
                ->join('cart','cart.barang_id','barang.id')
                ->join('users','cart.user_id','users.id')
                ->select('barang.nama_barang','barang.harga',
                    'barang.berat','barang.gambar','cart.jumlah','users.id as user_id'
                    ,DB::raw('barang.harga * cart.jumlah as total'),'barang.description')
                
                ->get();
        $totalBerat=[];
        $i=0;
                foreach($cartItem as $item){
                    
                       $totalBerat[$i]=$item->berat*$item->jumlah;
                   $i++;
                }
                $berat=array_sum($totalBerat);
          if($berat!=0){
			$data= RajaOngkir::Cost([
				'origin' 		=> 501, // id kota asal
				'destination' 	=> $request->kota_tujuan, // id kota tujuan
				'weight' 		=> $berat, // berat satuan gram
				'courier' 		=> $request->kurir, // kode kurir pengantar ( jne / tiki / pos )
			])->get();
		$a=$data[0]['costs'];
		$b=[];
	   for($i=0;$i<count($a);$i++){
		   $b[$i]=array_merge($a[$i],$a[$i]['cost'][0]);
		   unset($b[$i]['cost']);
	   }

	   return view('barang.checkout')->with('ongkir',$b);
		  
	}
               
       return redirect()->back(); 
        
    }
    public function delete($id)
    {

    }

    public function getCity()
    {
       return RajaOngkir::Kota()->all();
       
    }

    public function getProvince()
    {
        return RajaOngkir::Provinsi()->all();
    }
   
    public function getOngkir(Request $request)
    {
    //dd($request->all());   
        

          
           return redirect()->back()->with('ongkir',$b);
    }

}

