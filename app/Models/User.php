<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AccountApproved;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','cart'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'cart' => 'array'
    ];

    public function sendNotification()
    {
        $this->notify(new AccountApproved($this));
    }

    public function routeNotificationForOneSignal()
    {
        /*
         * you have to return the one signal player id tat will 
         * receive the message of if you want you can return 
         * an array of players id
         */

         return '8875eb7c-e6de-48a7-a5d4-f7f1a47d993a';
    }
}
