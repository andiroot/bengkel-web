<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table='order';

    protected $fillable=[
        'status','canceled_at','canceled_by',
        'confirmed_at','delivered','description',
        'phone','address'
    ];

    public $timestamps=true;
}
