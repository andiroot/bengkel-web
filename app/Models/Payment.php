<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table='payment';

    protected $fillable=[
        'payment_method','status','order_id',
        'total','total','total_paid','exchange',
        'discount','bukti_pembayaran','due_time',
        'paid_at'
    ];

    public $timestamps=true;
}
