<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    protected $table='barang';

    public $timestamps=true;

    protected $fillable=[
        'kode_barang','nama_barang',
        'stock','harga','gambar','created_at','updated_at'
    ];
}
