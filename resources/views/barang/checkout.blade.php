@extends('layouts.master')

@section('content')
<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="x_panel">
<div class="x_title">
<h2>New Partner Contracts Consultancy</h2>
<ul class="nav navbar-right panel_toolbox">
<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
<ul class="dropdown-menu" role="menu">
<li><a href="#">Settings 1</a>
</li>
<li><a href="#">Settings 2</a>
</li>
</ul>
</li>
<li><a class="close-link"><i class="fa fa-close"></i></a>
</li>
</ul>
<div class="clearfix"></div>
</div>
<div class="x_content">
<div class="col-md-9 col-sm-9 col-xs-12">
<ul class="stats-overview">
<li>
<span class="name"> Estimated budget </span>
<span class="value text-success"> 2300 </span>
</li>
<li>
<span class="name"> Total amount spent </span>
<span class="value text-success"> 2000 </span>
</li>
<li class="hidden-phone">
<span class="name"> Estimated project duration </span>
<span class="value text-success"> 20 </span>
</li>
</ul>
<br />
<div id="mainb" style="height:350px;" >
<form action="{{route('order.create')}}" method="post">
                
            {{ csrf_field() }}
    <div class="row">
        <div class="col-md-6 col-sm-5 col-xs-5">
        <div class="x_panel">
            
        <div class="x_content">
                
                <div class="form-group">
                        <label for="metode">Metode Pengiriman</label>
                        <table id="ongkir"  class="table">
                            
                            <thead>
                                <th>Layanan</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                                <th>Estimasi</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                
                                    @foreach ($ongkir as $o)
                                        <tr>
                                        <td>{{$o['service']}}</td>
                                        <td>{{$o['description']}}</td>
                                        <td>{{$o['value']}}</td>
                                        <td>{{$o['etd']}} Hari</td>
                                        <td><input type="radio" name="choose" id="choose" value ="{{$o['value']}}" required></td>
                                        </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                    </div>
        
        </div>
        </div>
       
        
    </div>
        
    <div class="col-md-6">
            <div class="x_panel">
                    <div class="x_content">
                            <div class="form-group">
                                    <label for="message">Alamat Pengiriman</label>
                                    <textarea id="message"  class="form-control" name="address" rows="5" required></textarea>
                            </div> 
                            <div class="form-group">
                                <label for="phone">Nomor yang dihubungi</label>
                                <input type="text" id="phone"  class="form-control" name="phone"required>
                        </div> 
                            
                        </div>
                    
                    
            </div>
        </div>
    
    
</div>

</div>

<div>
<h4>Recent Activity</h4>

<ul class="messages">


@foreach ($cartItem as $item)
<li>
        <img src="{{asset('storage/app/public/').$item->gambar}}" class="avatar" alt="Avatar">
        <div class="message_date">
        
        </div>
        <div class="message_wrapper">
        <h4 class="heading">{{$item->nama_barang}}</h4>
        <blockquote class="message">{{$item->description}}</blockquote>
        <br />
        
        </div>
        </li>
@endforeach

</ul>

</div>
</div>

<div class="col-md-3 col-sm-3 col-xs-12">
<section class="panel">
<div class="x_title">
<h2>Rincian Biaya</h2>
<div class="clearfix"></div>
</div>
<div class="panel-body">

<p>Total Belanja : {{$sum}}</p>

<br />

<div class="text-center mtop20">
<button class="btn btn-danger">Kembali</button>
<button type="submit" class="btn btn-success">Pembayaran</button>
</div>
</div>
</section>
</div>

</div>
</form>
</div>
</div>
</div>
</div>
</div>


@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

@endpush

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
$(document).ready(function() {
   
    if(document.getElementById('choose').checked) {

    }
    $('.js-example-basic-single').select2();


    $.ajax({
        type: 'GET',
        url: '/ongkir/city',

        success: function(data) {


            
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].city_id).text(data[i].city_name);


               
                $("#kota_tujuan").append(option);
            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });

    $.ajax({
        type: 'GET',
        url: '/ongkir/city',

        success: function(data) {


            
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].city_id).text(data[i].city_name);


                $("#kota_asal").append(option);
                
            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });
    $.ajax({
        type: 'GET',
        url: '/ongkir/province',

        success: function(data) {


           
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].province_id).text(data[i].province);


                $("#prov_tujuan").append(option);

            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });
});
</script>
<script>
jQuery('#cek').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
             
               jQuery.ajax({
                  url: "{{ url('/ongkir') }}",
                  method: 'post',
                  data: {
                     kota_tujuan: jQuery('#kota_tujuan').val(),
                     kurir: jQuery('#kurir').val(),
                     berat: {{$berat}},
                     _token: '{{ csrf_token() }}',
                  },
                  success: function(json){
                    
  $('#table_head').append('<td>Nama layanan</td><td>Deskripsi</td><td>Biaya</td>'+
        '<td>Estimasi waktu</td>');

                    console.log(Object.keys(json).length)
                    
                  }});
               });
</script>
@endpush