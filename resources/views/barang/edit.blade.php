@extends('layouts.master')

@section('content')


<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit
					<small>Barang</small>
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<form action="{{route('barang.update',$data->id)}}" method="post" class="form-horizontal form-label-left"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                  
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kode barang 
							<span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input value="{{$data->kode_barang}}" readonly id="kd_barang" name="kd_barang" class="form-control col-md-7 col-xs-12"  type="text">
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_barang">Nama barang 
								<span class="required"></span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input value="{{$data->nama_barang}}" id="nama_barang" name="nama_barang" class="form-control col-md-7 col-xs-12"  type="text">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Stock 
									<span class="required"></span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input value="{{$data->stock}}" id="stock_barang" name="stock_barang" class="form-control col-md-7 col-xs-12"  type="text">
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Harga 
										<span class="required"></span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input value="{{$data->harga}}" id="harga_barang" name="harga_barang" class="form-control col-md-7 col-xs-12"  type="text">
										</div>
									</div>
									<div class="item form-group">
										<label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea id="description" required="required" class="form-control" 
                                                                name="description" data-parsley-trigger="keyup" data-parsley-minlength="10" 
                                                                data-parsley-maxlength="50" 
                                                                data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." 
                                                                data-parsley-validation-threshold="10" placeholder="minimal 10 kata, maksimal 50 kata" required></textarea>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Gambar 
											<span class="required"></span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<img src="{{asset('storage/app/public/'.$data->gambar)}}" style="object-fit:contain;width:20%" >
												<br>
													<br>
														<input type="file" name="gambar_barang">
														</div>
													</div>
													<div class="ln_solid"></div>
													<div class="form-group">
														<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
															<button class="btn btn-danger" type="button">Cancel</button>
															<button class="btn btn-primary" type="submit">Update</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
        
          

@endsection