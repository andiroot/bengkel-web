@extends('layouts.master')

@section('content')

          <div class="clearfix"></div>
          <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Data<small>Barang</small></h2>
                      
                      <div class="clearfix"></div>
                    </div>
                    <form action="{{route('cart.checkout')}}" method="post">
                    <div class="x_content">
                      <div style="overflow-x:auto">
                            <div class="row">
                            
                             
                                    <div class="col-sm-3 mail_list_column">
                                    
                                   <br>
                                    <a href="#">
                                            <div class="mail_list">
                                            
                                            <div class="right">
                                                {{ csrf_field() }}
                      <div class="" style="width:200px">
                      <div class="form-group">
                        <label for="heard">Kota Asal</label>
                      <select id="kota_asal" name="kota_asal" class="form-control js-example-basic-single" required>
                        
                      </select>
                      </div>
                
                      <div class="form-group">
                      <label for="heard">Provinsi Tujuan</label>
                      <select id="prov_tujuan" name="provinsi_tujuan" class="form-control js-example-basic-single" required>
                      
                      </select>
                      </div>
                
                      <div class="form-group">
                      <label for="heard">Kota Tujuan</label>
                      <select id="kota_tujuan" name="kota_tujuan" class="form-control js-example-basic-single" required>
                      
                      </select>
                      </div>
                
                      <div class="form-group">
                      <label for="heard">Kurir</label>
                      <select id="kurir" name="kurir" class="form-control js-example-basic-single" required>
                        <option value="jne">JNE</option>
                              <option value="tiki">TIKI</option>
                              <option value="pos">POS INDONESIA</option>
                      </select>
                      </div>
                      
                      
                        
                        Berat Total : {{$berat}} gram.
                        
                        <div class="form-group">
                          Total Ongkir : 
                        </div>
                      <br>
                      <div class="form-group">
                        
                        <button id="compose" class="btn btn-sm btn-success btn-block" type="submit">LANJUT KE CHECKOUT</button>
                        
                      </div>
                    </form> 
        </div>
                                            
                                          
                                            
                                          </div>
                                            </div>
                                          
                                    </a>
                                    
                                       
                                            
                                    
                                    </div>
                                    
                                    
                                    <div class="col-sm-9 mail_view">
                                    <div class="inbox-body">
                                    <div class="mail_heading row">
                                    <div class="col-md-8">
                                    
                                    </div>
                                    <div class="col-md-4 text-right">
                                    
                                    </div>
                                    <div class="col-md-12">
                                    <h4> Daftar Barang Yang Ada Di Keranjang <small class="pull-right"><strong>Total Belanja : Rp {{number_format($sum,2,',','.')}}</strong> </small></h4>
                                    </div>
                                    </div>
                                    <ul class="messages">
                                            
                                            @foreach ($items as $item)
                                            <li>
                                                    <img src="{{asset('/storage/app/public/'.$item->gambar)}}" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                    <h3 class="date text-info">Rp {{number_format($item->total,2,',','.')}}</h3>
                                                    <p class="month">Jumlah item {{$item->jumlah}} pcs</p>
                                                    <p class="month">@ Rp {{number_format($item->harga,2,',','.')}}
                                                    <br>
                                                    
                                                    </p>
                                                   
                                                    </div>
                                                    
                                                    <div class="message_wrapper">
                                                    <h4 class="heading">{{$item->nama_barang}}</h4>
                                                    <blockquote class="message">{{$item->description}}</blockquote>
                                                    <br />
                                                    
                                                           
                                                                            
                                                    
                                                                                    <button type="button" class="btn btn-default minus"><i class="fa fa-trash" style="color:red"></i> Hapus</button>
                                                                                    <button type="button" class="btn btn-default minus"><i class="fa fa-edit" style="color:darkorange"></i> Edit</button>
                                                                                    
                                                    </div>
                                                    </li>
                                            @endforeach
                                            
                                            </ul>
                                    </div>
                                    
                                    
                                    </div>
                                    </div>
                     
                    </div>

                  </div>
                  </div>
                </div>
          </div>
        </div>
        
        


@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

@endpush

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();


    $.ajax({
        type: 'GET',
        url: '/ongkir/city',

        success: function(data) {


            
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].city_id).text(data[i].city_name);


               
                $("#kota_tujuan").append(option);
            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });

    $.ajax({
        type: 'GET',
        url: '/ongkir/city',

        success: function(data) {


            
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].city_id).text(data[i].city_name);


                $("#kota_asal").append(option);
                
            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });
    $.ajax({
        type: 'GET',
        url: '/ongkir/province',

        success: function(data) {


            console.log(data);
            var option = "";
            var html = '';
            for (i = 1; i < data.length; i++) {
                option = $('<option></option>').attr("value", data[i].province_id).text(data[i].province);


                $("#prov_tujuan").append(option);

            }


            //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten 

        }
    });
});
</script>
@endpush