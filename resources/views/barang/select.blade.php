

@extends('layouts.master') 


@section('content')


<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
    <div class="x_title">
    <h2>E-commerce page design</h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
    <ul class="dropdown-menu" role="menu">
    <li><a href="#">Settings 1</a>
    </li>
    <li><a href="#">Settings 2</a>
    </li>
    </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
    </ul>
    <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <div class="col-md-7 col-sm-7 col-xs-12">
    <div class="product-image">
    <img src="images/prod-1.jpg" alt="..." />
    </div>
    <div class="product_gallery">
    <a>
    <img src="images/prod-2.jpg" alt="..." />
    </a>
    <a>
    <img src="images/prod-3.jpg" alt="..." />
    </a>
    <a>
    <img src="images/prod-4.jpg" alt="..." />
    </a>
    <a>
    <img src="images/prod-5.jpg" alt="..." />
    </a>
    </div>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">
    <h3 class="prod_title">{{$data->nama_barang}}</h3>
    <p>{{$data->description}}</p>
    <br />
   
    <br />
    <div class="">
    <h2><small>Jumlah barang</small></h2>
    <form action="{{route('cart.add')}}" method="post">
    <div class="input-group">
        {{ csrf_field() }}
    <input type="hidden" name="barang" value="{{$data->id}}">
        <span class="input-group-btn">
        <button type="button" class="btn btn-default minus"><i class="fa fa-minus"></i></button>
        </span>
        <input type="number" value="1" name="jumlah" class="form-control count" style="text-align:center ;width:150px !important" readonly>
        
        <span class="input-group-btn">
                <button type="button" class="btn btn-default plus" style="margin-right: 260px !important"><i class="fa fa-plus"></i></button>
                </span>
        </div>
    </div>
    <br />
    <div class="">
    <div class="product_price">
    <h1 class="price">Rp {{number_format($data->harga,2,',','.')}}</h1>
    <span class="price-tax">Harga sudah termasuk pajak</span>
    <br>
    </div>
    </div>
    
    <div class="">
    <button type="submit" class="btn btn-default source " id="tambah">
     <i class="fa fa-cart-plus" ></i> Tambah ke Keranjang    
    </button>
</form>
    
    <button type="button" class="btn btn-default "><i class="fa fa-heart-o"></i> Tambah ke Wishlist</button>
    </div>
    <div class="product_social">
    <ul class="list-inline">
    <li><a href="#"><i class="fa fa-facebook-square"></i></a>
    </li>
    <li><a href="#"><i class="fa fa-twitter-square"></i></a>
    </li>
    <li><a href="#"><i class="fa fa-envelope-square"></i></a>
    </li>
    <li><a href="#"><i class="fa fa-rss-square"></i></a>
    </li>
    </ul>
    </div>
    </div>
    <div class="col-md-12">
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Home</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Profile</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
    </li>
    </ul>
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
    <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher
    synth. Cosby sweater eu banh mi, qui irure terr.</p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
    booth letterpress, commodo enim craft beer mlkshk aliquip</p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
    <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
    photo booth letterpress, commodo enim craft beer mlkshk </p>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    </div>
    </div>
    </div>
    </div>
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    
@endif
@endsection


@push('css')
    <style>
    .qty .count {
    color: #000;
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    font-weight: 700;
    line-height: 30px;
    padding: 0 2px
    ;min-width: 35px;
    
    
}
.qty .plus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    
    border-radius: 50%;
    }
.qty .minus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    
    border-radius: 50%;
    background-clip: padding-box;
}

.minus:hover{
    background-color: #41B99A !important;
}
.plus:hover{
    background-color: #41B99A !important;
}
/*Prevent text selection*/
span{
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
input{  
    border: 0;
    width: 2%;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input:disabled{
    background-color:white;
}
    </style>

<link href="{{asset('/gentelella/vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
<link href="{{asset('/gentelella/vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
<link href="{{asset('/gentelella/vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
@endpush

@push('js')

<script>

$(document).ready(function(){
		    
   			$(document).on('click','.plus',function(){
				$('.count').val(parseInt($('.count').val()) + 1 );
    		});
        	$(document).on('click','.minus',function(){
    			$('.count').val(parseInt($('.count').val()) - 1 );
    				if ($('.count').val() == 0) {
						$('.count').val(1);
					}
    	    	});
 		});

</script>
<script src="{{asset('/gentelella/vendors/pnotify/dist/pnotify.js')}}" type="d5fa1b09aa9c5dcdf2a0601f-text/javascript"></script>
<script src="{{asset('/gentelella/vendors/pnotify/dist/pnotify.buttons.js')}}" type="d5fa1b09aa9c5dcdf2a0601f-text/javascript"></script>
<script src="{{asset('/gentelella/vendors/pnotify/dist/pnotify.nonblock.js')}}" type="d5fa1b09aa9c5dcdf2a0601f-text/javascript"></script>
@endpush