@extends('layouts.master')

@section('content')

          <div class="clearfix"></div>
          <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Data<small>Barang</small></h2>
                      
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div style="overflow-x:auto">
                      <table id="datatable" class="table table-striped table-responsive">
                        <thead>
                          
                          <tr>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Stock</th>
                            <th>Harga</th>
                            <th style="text-align:center">Gambar</th>
                            <th style="text-align:center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($data as $d)
                            <tr>
                              <td>{{$d->kode_barang}}</td>
                              <td>{{$d->nama_barang}}</td>
                              <td>{{$d->stock}}</td>
                              <td>{{$d->harga}}</td>
                              <td style="width:5%;text-align:center"><img src="{{asset('storage/app/public/'.$d->gambar)}}" style="width:1cm;height:1cm
                              object-fit: contain; "></td>
                              <td style="text-align:center">
                                      <a href="{{route('barang.edit',$d->id)}}" class="btn btn-default btn-xs" style="color:darkorange"> <i class="fa fa-edit"></i> Edit</a>
                             
                                <form method="POST" action="{{route('barang.destroy',$d->id)}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-default btn-xs" style="color:red" onclick="return confirm('Are you sure?')"> <i class="fa fa-trash"> Hapus</i></button>
                                </form>    
                              </td>
                             
                            </tr>
                           @endforeach
                        </tbody>
                      </table>
                     
                    </div>
                    {{ $data->links() }}
                  </div>
                  </div>
                </div>
          </div>
        </div>
        
        <a href="#" class="float" data-toggle="modal" data-target="#barang">
                <i class="fa fa-plus my-float"></i>
                </a>
                <div class="label-container">
                <div class="label-text">Tambah Barang</div>
                <i class="fa fa-play label-arrow"></i>
                </div>

<!-- Modal -->
<div id="barang" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body">
            <form action="{{route('barang.store')}}" method="post" class="form-horizontal form-label-left"  enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kode barang <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="kd_barang" name="kd_barang" class="form-control col-md-7 col-xs-12" required="required" type="text">
                            </div>
                    </div>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_barang">Nama barang <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="nama_barang" name="nama_barang" class="form-control col-md-7 col-xs-12" required="required" type="text">
                            </div>
                    </div>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Stock <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="stock_barang" name="stock_barang" class="form-control col-md-7 col-xs-12" required="required" type="text">
                            </div>
                    </div>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Harga <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="harga_barang" name="harga_barang" class="form-control col-md-7 col-xs-12" required="required" type="text">
                            </div>
                    </div>
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Berat <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="berat" name="berat" class="form-control col-md-7 col-xs-12" required="required" type="text">
                      </div>
              </div>
                    <div class="item form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="description" required="required" class="form-control" 
                                                                    name="description" data-parsley-trigger="keyup" data-parsley-minlength="10" 
                                                                    data-parsley-maxlength="50" 
                                                                    data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." 
                                                                    data-parsley-validation-threshold="10" placeholder="minimal 10 kata, maksimal 50 kata" required></textarea>
                        </div>
                      </div>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_barang">Gambar <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="gambar_barang" name="gambar_barang" class=" " required="required" type="file">
                            </div>
                    </div>
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cancel</button>
                    <button id="send" type="submit" class="btn btn-success">Submit</button>
                    
            </div>
          </div>
        </form>
        </div>
      </div>


@endsection

@push('css')
<link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet">

<style>
*{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	background-color:#CCC;
	font-size:12px;
}

.label-container{
	position:fixed;
	bottom:48px;
	right:105px;
	display:table;
	visibility: hidden;
}

.label-text{
	color:black;
	background:rgba(51,51,51,0.5);
	display:table-cell;
	vertical-align:middle;
	padding:10px;
	border-radius:3px;
}

.label-arrow{
	display:table-cell;
	vertical-align:middle;
	color:#333;
	opacity:0.5;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:30px;
	right:40px;
	background-color:#1ABB9C;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	font-size:24px;
	margin-top:18px;
}

a.float + div.label-container {
  visibility: hidden;
  opacity: 0;
  transition: visibility 0s, opacity 0.5s ease;
}

a.float:hover + div.label-container{
  visibility: visible;
  opacity: 1;
}

</style>
@endpush

@push('js')

@endpush