@extends('layouts.master')

@section('content')

<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="x_panel">
<div class="x_title">
    <div class="x_content">
            <div class="row"><br>
                   @foreach ($data as $d)
                   <div class="col-md-55">
                        <div class="thumbnail">
                        <div class="image view view-first">
                        <img style="width: 100%; display: block;" src="{{asset('storage/app/public/'.$d->gambar)}}" alt="image" />
                        <div class="mask">
                       
                        <div class="tools tools-bottom">
                        <a href="{{route('browse.select',$d->id)}}"><i class="fa fa-plus"></i></a>
                        
                        </div>
                        </div>
                        </div>
                        <div class="caption">
                                <p>Rp <strong>{{number_format($d->harga,2,',','.')}}</strong>
                                </p>
                            <p>{{$d->nama_barang}}</p>
                            
                                </div>

                        </div>
                        </div>
                   @endforeach
                    
            </div>
    </div>
    </div>
</div>
</div>
</div>
</div>


@endsection

