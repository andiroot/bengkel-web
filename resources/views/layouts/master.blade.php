<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>



    <!-- Bootstrap -->
    <link href="{{asset('/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('/gentelella/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('/gentelella/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="{{asset('/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="{{asset('/gentelella/build/css/custom.css')}}" rel="stylesheet">
    @stack('css')

  

  </script>
  

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                
              </div>
              <div class="profile_info">
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            @if (Auth::user()==null)
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                <div class="menu_section">
                  <h3>General</h3>
                  <ul class="nav side-menu">
                    <li><a>
                      <label for="">Kategori</label>  
                      <select class="form-control" name="" id="">
                        <option value="">Electronic</option></select> </a>
                     <ul class="nav child_menu">
                       
                     </ul>
                    </li>
                    
                    
  
                    
                    
                    
                  </ul>
                </div>
                
  
              </div>
            @else
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                <div class="menu_section">
                  <h3>General</h3>
                  <ul class="nav side-menu">
                  <li><a href="{{route('browse')}}"><i class="fa fa-home"></i> Home </a>
                     <ul class="nav child_menu">
                       
                     </ul>
                    </li>
                    @if (Auth::user()->email=='admin@admin.com')
                    <li><a><i class="fa fa-edit"></i> Barang <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="{{route('barang.index')}}">Data Barang</a></li>
                        
                      </ul>
                    </li>                      
                    @endif
  
                    
                    <li><a><i class="fa fa-desktop"></i> Pesanan <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                          @if (Auth::user()->email=='admin@admin.com')
                          <li><a href="{{route('order.get')}}">Data Pesanan</a></li>
                          @else
                          <li><a href="{{route('order.index')}}">Data Pesanan</a></li>
                          @endif
                      
                        
                      </ul>
                    </li>
                    @if (Auth::user()->email=='admin@admin.com')
                        
                    <li><a><i class="fa fa-group"></i> Customer <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="#">Data Customer</a></li>
                        
                      </ul>
                    </li>
                    @endif
  
                    
                    
                    
                  </ul>
                </div>
                
  
              </div>
            @endif
            
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav" >
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              @if (Auth::user()==null)
              <ul class="nav navbar-nav navbar-right">
                

                  <li role="presentation" class="dropdown">
                    
                    
                    
                  </li>
                  <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                     
                    <i class="fa fa-shopping-cart"><span class="badge bg-red" id="carts">0</span></i> 
                      
                      
                      <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        
                        
                          <li>
                              <div class="text-center">
                              <a href="#">
                                  <strong>Keranjang kosong</strong>
                                  
                                </a>
                              </div>
                            </li>
                        
                      </ul>
                      
                    </a>
                    
                  </li>
                  
                </ul>
              @else
              <ul class="nav navbar-nav navbar-right">
                  <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      {{Auth::user()->name}}
                      <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                      
                      
                      <li><a href="javascript:;">Help</a></li>
                      <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                  </li>
  
                  
                  <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                      @if ($cartItem===null || $countCart===0)
                      <i class="fa fa-shopping-cart"></i>      
                      @else
                    <i class="fa fa-shopping-cart"><span class="badge bg-red" id="carts">{{$countCart}}</span></i> 
                      @endif
                      
                      <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li>
                          @foreach ($cartItem as $item)
                          <a>
                              <span class="image"><img src="{{asset('storage/app/public/'.$item->gambar)}}" alt="Profile Image" /></span>
                              <span>
                                <span>{{$item->nama_barang}}</span>
                              <span class="time">{{$item->jumlah}} pcs</span>
                              </span>
                              <span class="message">
                                  Rp {{number_format($item->total,2,',','.')}}
                              </span>
                            </a>
                          @endforeach
                         
                        </li>
                        @if (count($cartItem)!=0)
                        <li>
                            <div class="text-center">
                            <a href="{{route('cart.index')}}">
                                <strong>Lihat Detail</strong>
                                <i class="fa fa-angle-right"></i>
                              </a>
                            </div>
                          </li>
                          @else
                          <li>
                              <div class="text-center">
                              <a href="#">
                                  <strong>Keranjang kosong</strong>
                                  
                                </a>
                              </div>
                            </li>
                        @endif
                      </ul>
                      
                    </a>
                    
                  </li>
                  
                </ul> 
              @endif
              
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            @yield('content')
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          
        </footer>
        <!-- /footer content -->
      </div>
    </div>

   

    <!-- jQuery -->
<script src="{{asset('/gentelella/vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Popper -->
<script src="{{asset('/gentelella/vendors/popper/popper.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('/gentelella/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('/gentelella/vendors/nprogress/nprogress.js')}}"></script>
<!-- jQuery custom content scroller -->
<script src="{{asset('/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('/gentelella/build/js/custom.min.js')}}"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
@stack('js')
  </body>
</html>