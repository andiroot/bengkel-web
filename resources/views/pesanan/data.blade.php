@extends('layouts.master')

@section('content')

<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="x_panel">
<div class="x_title">
    <div class="x_content">
            <div class="row"><br>
                   <table class="table" id="table">
                       <thead>
                           <th>Order ID</th>
                           <th>Status</th>
                           <th>Dibatalkan Pada</th>
                           <th>Dikirim Pada</th>
                           <th>Dibuat Pada</th>
                           <th>Metode Pembayaran</th>
                           <th>Status Pembayaran</th>
                           <th>Total Tagihan</th>
                           
                          
                           <th>Bukti Pembayaran</th>
                           <th>Batas Waktu Pembayaran</th>
                           <th>Dibayar Pada</th>
                           <th>Phone</th>
                           
                           
                           <th>E-mail</th>
                           <th>Name</th>
                           <th>Alamat</th>
                       </thead>
                       
                   </table>
                  
            </div>
            
    </div>
    </div>
</div>
</div>
</div>
</div>


@endsection

@push('js')

<script src="{{asset('gentelella/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}" ></script>

<script>
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                scrollXInner: true,
                
                responsive:false,
                ajax: '{!! route('order.getOrder') !!}',
                columns: [
                    {data:'order_id',name:'order_id'},
                    {data: 'order_status', name: 'order_status'},
                    {data:'canceled_at',name:'canceled_at'},
                    {data: 'delivered',name:'delivered', searchable:false},
                    {data:'created_at',name:'created_at'},
                    {data: 'payment_method', name: 'payment_method'},
                    {data:'payment_status',name:'payment_status'},
                    {data:'total',name:'total'},
                    
                    {data: 'bukti_pembayaran', name: 'bukti_pembayaran'},
                    {data:'due_time',name:'due_time'},
                    {data: 'paid_at',name:'paid_at', searchable:false},
                    {data: 'phone', name: 'phone'},
                    {data:'email',name:'email'},
                    {data: 'name',name:'name', searchable:false},
                    {data: 'address',name:'address', searchable:false},
                    
                    
                ],
            
        });
    });  
       
    </script>
   
@endpush
