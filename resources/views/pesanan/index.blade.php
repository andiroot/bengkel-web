@extends('layouts.master')

@section('content')

<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="x_panel">
<div class="x_title">
    <div class="x_content">
            <div class="row"><br>
                   <table class="table">
                       <thead>
                           <th>Order ID</th>
                           <th>Status</th>
                           <th>Dibatalkan Pada</th>
                           <th>Dikirim Pada</th>
                           <th>Dibuat Pada</th>
                           <th>Metode Pembayaran</th>
                           <th>Status Pembayaran</th>
                           <th>Total Tagihan</th>
                           <th>Bukti Pembayaran</th>
                           <th>Batas Waktu Pembayaran</th>
                           <th>Dibayar Pada</th>
                       </thead>
                       <tbody>
                            
                                @foreach ($data as $d)
                                <tr>
                                <td><a href="{{route('order.invoice',$d->order_id)}}">{{$d->order_id}}</a></td>
                            <td>{{$d->order_status}}</td>
                            <td>{{$d->canceled_at}}</td>
                                <td>{{$d->delivered}}</td>
                                <td>{{$d->created_at}}</td>
                                <td>{{$d->payment_method}}</td>
                                
                                <td>{{$d->payment_status}}</td>
                                <td>Rp {{number_format($d->total,2,',','.')}}</td>
                                <td>{{$d->bukti_pembayaran}}</td>
                                
                                <td>{{$d->due_time}}</td>
                                <td>{{$d->paid_at}}</td>
                        </tr>
                                @endforeach
                            
                       </tbody>
                   </table>
                   {{ $data->links() }}
            </div>
            
    </div>
    </div>
</div>
</div>
</div>
</div>


@endsection

