@extends('layouts.master')

@section('content')

<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="x_panel">
<div class="x_title">
    <div class="x_content">
        <section class="content invoice" id="print">

            <div class="row">
            <div class="col-xs-12 invoice-header">
            <h1>
            <i class="fa fa-globe"></i> Invoice.
            <small class="pull-right">Date: 16/08/2016</small>
            </h1>
            </div>
            
            </div>
            
            <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
            From
            <address>
            <strong>Iron Admin, Inc.</strong>
            <br>795 Freedom Ave, Suite 600
            <br>New York, CA 94107
            <br>Phone: 1 (804) 123-9876
            <br>Email: ironadmin.com
            </address>
            </div>
            
            <div class="col-sm-4 invoice-col">
            To
            <address>
            <strong>{{$order->name}}</strong>
            <br>{{$order->address}}
             <br>Phone: {{$order->phone}}
            <br>Email: <a href="#" class="__cf_email__" data-cfemail="adc7c2c3edc4dfc2c3ccc9c0c4c383cec2c0">{{$order->email}}</a>
            </address>
            </div>
           
            <div class="col-sm-4 invoice-col">
            <b>Invoice #{{str_pad($order->order_id, 6, '0', STR_PAD_LEFT)}}</b>
            
            
            
            <br>
            <b>Payment Due:</b> {{$order->due_time}}
            <br>
            <b>Account:</b> 968-34567
            </div>
            
            </div>
            
            
            <div class="row">
            <div class="col-xs-12 table">
            <table class="table table-striped">
            <thead>
            <tr>
            
            <th>Product</th>
            
            <th style="width: 60%">Description</th>
            <th style="width:5%">Qty</th>
            <th style="width:10%">Subtotal</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($items as $item)
                <tr>
                
                <td>{{$item->nama_barang}}</td>
               
                <td>{{$item->description}}</td>
                <td>{{$item->jumlah}} pcs</td>
                <td>Rp {{number_format($item->total,2,',','.')}}</td>
                </tr>
            @endforeach
            </tbody>
            </table>
            </div>
            
            </div>
            
            <div class="row">
            
            <div class="col-xs-6">
            <p class="lead">Payment Methods:</p>
            
            
            
              
                <div class="text-muted well well-sm no-shadow">
                    @if ($order->payment_status=='waiting')
                    <form action="{{route('order.paid')}}" method="post" enctype="multipart/form-data" >
                        <u>Transfer Bank</u> <br>
                        {{ csrf_field() }}
                <input type="hidden" name="order" value="{{$order->order_id}}">
                    <label for="">Upload Bukti Pembayaran</label> <input type="file" name="bukti" id=""> <br> <button type="submit" class="btn btn-primary">Upload</button></p>
                </form>
                    @elseif($order->payment_status=='processing')
                        Terima kasih, bukti pembayaran anda sedang kami proses harap tunggu 1x24 jam
                        @elseif($order->payment_status=='expired')
                        Maaf , pesanan anda telah melewati batas waktu pembayaran
                    @else
                    Terima kasih telah melakukan pembayaran untuk pesanan anda
                        @endif
                </div>
                
                <p>
                
            
            </div>
            
            <div class="col-xs-6">
            <p class="lead">Amount Due {{$order->due_time}}</p>
            <div class="table-responsive">
            <table class="table">
            <tbody>
            <tr>
            <th style="width:50%">Subtotal:</th>
            <td>Rp {{number_format($sum,2,',','.')}}</td>
            </tr>
            <tr>
            <th>Shipping:</th>
            <td>Rp {{number_format($order->ongkir,2,',','.')}}</td>
            </tr>
            <tr>
            <th>Total:</th>
            <td>Rp {{number_format($sum+$order->ongkir,2,',','.')}}</td>
            </tr>
            </tbody>
            </table>
            </div>
            </div>
            
            </div>
            
            
            <div class="row no-print">
            <div class="col-xs-12">
            
            </div>
            </div>
            </section>
            
    </div>
    </div>
</div>
</div>
</div>
</div>


@endsection

@push('js')
    <script>
    function print(){

        $('#print').printElement();
        window.print(); -
    }
    
    </script>
@endpush
