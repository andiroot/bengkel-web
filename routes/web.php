<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    
    return redirect('/browse');
});
Route::group(['prefix'=>'browse'], function(){
    Route::get('/','BarangController@browse')->name('browse');
    Route::get('/{id}','BarangController@select')->name('browse.select');
});

Route::group(['middleware' => ['auth', 'web']], function (){
    Route::resource('barang','BarangController', ['parameters' => [
        'barang' => 'id']]);
    
    
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::group(['prefix'=>'ongkir'], function(){
        Route::get('/city','CartController@getCity')->name('ongkir.city');
        Route::get('/province','CartController@getProvince')->name('ongkir.provinsi');
        Route::post('/','CartController@getOngkir')->name('ongkir.get');
    });
    
    Route::group(['prefix'=>'cart'], function (){
        Route::get('/','CartController@index')->name('cart.index');
        Route::post('/','CartController@add')->name('cart.add');
        Route::post('/{id}','CartController@delete')->name('cart.delete');
        
    });
    Route::post('/checkout','CartController@checkout')->name('cart.checkout');
    
    Route::group(['prefix'=>'order'], function(){
        Route::post('/','OrderController@create')->name('order.create');
        Route::get('/','OrderController@myOrder')->name('order.index');
        Route::get('/getOrder','OrderController@getOrder')->name('order.getOrder');
        Route::get('/invoice/{id}','OrderController@invoice')->name('order.invoice');
        Route::post('/paid','OrderController@paid')->name('order.paid');
        Route::get('/get','OrderController@get')->name('order.get');
        
        
    });
});




Auth::routes();


